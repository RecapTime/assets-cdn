# Recap Time Squad's Static Assets CDN

Accessible through `cdn.recaptime.eu.org`/`cdn.rtapp.tk` (legacy) and also `static.rtdevcdn.net.eu.org`, hosted through Cloudflare Pages.

## Adding an Image

1. [Fork the repo](https://gitlab.com/RecapTime/assets-cdn/fork) into your account.
2. Upload the photos to `/public` directory. If don't know where to categorize it, just drop it there, and we'll
handle the rest.

## Policy

* Follow the image license. If the copyright owner don't like reuploaded their work here, please DO NOT COMMIT IT!
Otherwise, you're in danger of getting susupended for DMCA violations. (Instead, use the publicly-available link for use as redirect instead.)
